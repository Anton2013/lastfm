# -*- coding: utf-8 -*-
__author__ = 'anton'
from flask import Blueprint, render_template, request, session, redirect, url_for
from config import Config
from extensions import oid
import helpers

app_mainpage = Blueprint('Mainpage', __name__)


@app_mainpage.route('/authorization/', methods=['GET', 'POST'])
@oid.loginhandler
def authorization():
    if request.method == 'POST':
        openid = 'https://www.google.com/accounts/o8/id'
        session['username'] = request.form['name']
        return oid.try_login(openid, ask_for=['email'])
    return render_template('register.html', next=oid.get_next_url(), error=oid.fetch_error())


@oid.after_login
def create_or_login(resp):
    session['openid'] = resp.identity_url
    session['email'] = resp.email
    return redirect(url_for('.main_page', next=oid.get_next_url(),
                            name=resp.nickname,
                            email=resp.email))


@app_mainpage.route('/user/')
def user():
    if request.method == 'POST':
        session['name'] = request.form['name']
    return render_template('user.html')


@app_mainpage.route('/friends_hitparade/', methods=['GET', 'POST'])
def friends_page():
    name_friend = session['name']
    exist_user = helpers.check_user(name_friend)
    if not exist_user:
        helpers.parser_run(name_friend)
    artist_hit_parade, track_hit_parade = helpers.get_artist_hit_parade(name_friend), \
                                          helpers.get_track_hit_parade(name_friend)
    return render_template('hitparad.html', artist_hit_parade=sorted(artist_hit_parade, reverse=True),
                           user=name_friend.decode('utf-8'), limit=Config.NUMBER_USERS,
                           track_hit_parade=sorted(track_hit_parade, reverse=True))


@app_mainpage.route('/', methods=['GET', 'POST'])
def main_page():
    if 'openid' not in session:
        return redirect('/authorization/')
    username = session['username']
    exist_user = helpers.check_user(username)
    if not exist_user:
        helpers.parser_run(username)
    artist_hit_parade, track_hit_parade = helpers.get_artist_hit_parade(username), \
                                          helpers.get_track_hit_parade(username)
    return render_template('hitparad.html', artist_hit_parade=sorted(artist_hit_parade, reverse=True),
                           user=username.decode('utf-8'), limit=Config.NUMBER_USERS,
                           track_hit_parade=sorted(track_hit_parade, reverse=True))


@app_mainpage.route('/big_hitparade_artist/<name>', methods=['GET', 'POST'])
def big_hit_parade_artist(name):
    artist_hit_parade = helpers.get_artist_hit_parade(name)
    return render_template('child_hitparade_artist.html', artist_hit_parade=sorted(artist_hit_parade, reverse=True),
                           user=name.decode('utf-8'), limit=Config.NUMBER_USERS)


@app_mainpage.route('/big_hitparade_track/<name>', methods=['GET', 'POST'])
def big_hit_parade_track(name):
    track_hit_parade = helpers.get_track_hit_parade(name)
    return render_template('child_hitparade_track.html', track_hit_parade=sorted(track_hit_parade, reverse=True),
                           user=name.decode('utf-8'), limit=Config.NUMBER_USERS)


@app_mainpage.route('/download_artist/<name>', methods=['GET', 'POST'])
def download_artist(name):
    artist_hit_parade = helpers.get_artist_hit_parade(name)
    response = helpers.artist_to_download(artist_hit_parade)
    return response


@app_mainpage.route('/download_tracks/<name>', methods=['GET', 'POST'])
def download_tracks(name):
    track_hit_parade = helpers.get_track_hit_parade(name)
    response = helpers.track_to_download(track_hit_parade)
    return response