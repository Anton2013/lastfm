# -*- coding: utf-8 -*-
__author__ = 'anton'
from extensions import db

neighbours = db.Table('neighbours',
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
    db.Column('neighbor_id', db.Integer, db.ForeignKey('users.id'))
)


class Users(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(256), unique=True)
    relations = db.relationship('Users', secondary=neighbours,
                                primaryjoin=(neighbours.c.user_id == id),
                                secondaryjoin=(neighbours.c.neighbor_id == id),
                                backref=db.backref('neighbours', lazy='dynamic'),
                                lazy='dynamic')

    def __unicode__(self):
        return "Name of user is %s" % self.username


class UserHitParade(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    artist = db.Column(db.String(256))
    value = db.Column(db.Integer)

    def __unicode__(self):
        return "Artist %s have score %i" % (self.artist, self.value)


class UserTrackParade(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    track = db.Column(db.String(256))
    value = db.Column(db.Integer)
    url = db.Column(db.String(256))
    artist = db.Column(db.String(256))

    def __unicode__(self):
        return "Track %s have score %i" % (self.track, self.value)

