# -*- coding: utf-8 -*-
__author__ = 'anton'
import pylast
import sys
from config import Config

connect = pylast.LastFMNetwork(api_key=Config.API_KEY, api_secret=Config.API_SECRET)


def get_neighbors(username):
    user = pylast.User(username, connect)
    neighbour = user.get_neighbours()
    new_neighbors = [username]
    try:
        for i in xrange(0, Config.NUMBER_USERS):
            new_neighbors.append(neighbour[i].get_name())
    except IndexError:
        print 'This user has not enough neighbors. Try again or change Config'
        sys.exit()
    return new_neighbors


def make_artist_parade(neighbours):
    """ Create hitparade for each user"""
    top_artist_by_users = {}
    for key in neighbours:
        user = pylast.User(key, connect)
        top_artist = user.get_top_artists()
        artist_value = {}
        for i in xrange(0, Config.LIMIT):
            artist_value[top_artist[i].item.get_name()] = top_artist[i].weight
        top_artist_by_users[key] = artist_value
    return top_artist_by_users


def make_track_parade(neighbours):
    top_tracks_by_users = {}
    for key in neighbours:
        user = pylast.User(key, connect)
        top_tracks = user.get_top_tracks()
        track_value = {}
        for i in xrange(0, Config.TRACK_LIMIT):
            try:
                name_track = top_tracks[i].item.get_name().encode('utf-8')
            except:
                beta_track = top_tracks[i].item.get_name().decode('utf-8')
                name_track = beta_track.encode('utf-8')
            try:
                artist = unicode(top_tracks[i].item.get_artist()).encode('utf-8')
            except:
                beta = str(top_tracks[i].item.get_artist()).decode('utf-8')
                artist = beta.encode('utf-8')
            url = 'http://vk.com/audio?q=' + artist + ' ' + name_track
            track_value[name_track] = (top_tracks[i].weight, url, artist)
        top_tracks_by_users[key] = track_value
    return top_tracks_by_users