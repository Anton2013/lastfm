# -*- coding: utf-8 -*-
__author__ = 'anton'
from flask import Flask
from views import app_mainpage
from extensions import db, oid
from config import Config

# init flask
app = Flask(__name__, template_folder='../templates', static_folder='../static')
app.config.from_object(Config)
oid.init_app(app)
app.secret_key = Config.API_SECRET_KEY
#init db
db.app = app
db.init_app(app)
db.create_all()

# blueprints
app.register_blueprint(app_mainpage)



