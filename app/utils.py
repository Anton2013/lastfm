__author__ = 'anton'
from pprint import pprint


def print_obj(object_name):
	return pprint({ x: getattr(object_name, x) for x in dir(object_name)})