# -*- coding: utf-8 -*-
__author__ = 'anton'

from flask_sqlalchemy import SQLAlchemy
from flask_openid import OpenID

db = SQLAlchemy()
oid = OpenID()
