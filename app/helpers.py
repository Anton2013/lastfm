# -*- coding: utf-8 -*-
__author__ = 'anton'
from parser import get_neighbors, make_artist_parade, make_track_parade
from extensions import db
from sqlalchemy import exists
from models import UserHitParade, Users, neighbours, UserTrackParade
from flask import make_response, send_file
import csv


def input_users(neighbours, username):
    names = []
    user_not_in_track_table = []
    relation = []
    current_user = db.session.query(Users).filter(Users.username == username).first()
    if not current_user:
        current_user = Users(username=username)
        db.session.add(current_user)
        db.session.commit()
    for i in neighbours:
        # make relationships
        if i != username:
            user = db.session.query(Users).filter(Users.username == i).first()  # (id, username)
            if not user:
                user = Users(username=i)
                names.append(i)
                db.session.add(user)
                db.session.commit()
            relation.append(user)
            # make list of users who artist and tracks not in our tables
        user = db.session.query(Users).filter(Users.username == i).first()
        exist_in_tracks_table = db.session.query(exists().where(UserTrackParade.user_id == user.id)).first()
        if not exist_in_tracks_table[0]:
            user_not_in_track_table.append(i)

    current_user.relations = relation
    db.session.commit()
    return names, user_not_in_track_table


def input_values(artist_top, track_top):
    for user in artist_top:
        user_id = db.session.query(Users.id).filter(Users.username == user).first()  # (id,)
        for artist, value in sorted(artist_top[user].items(), key=lambda (k, v): v, reverse=True):  # sort dict
            values = UserHitParade(user_id=user_id[0], artist=artist, value=value)
            db.session.add(values)
    for user in track_top:
        user_id = db.session.query(Users.id).filter(Users.username == user).first()  # (id,)
        for track, value in sorted(track_top[user].items(), key=lambda (k, v): v, reverse=True):
            values = UserTrackParade(user_id=user_id[0], track=track, value=value[0], url=value[1], artist=value[2])
            db.session.add(values)
    db.session.commit()


def parser_run(name):
    #init parser
    new_neighbors = get_neighbors(username=name)
    # get only new names
    artist_names, track_names = input_users(neighbours=new_neighbors, username=name)
    # parse only new names
    artist_top, track_top = make_artist_parade(neighbours=artist_names), make_track_parade(neighbours=track_names)
    #input values in db
    input_values(artist_top, track_top)


def check_user(name):
    user_exist = db.session.query(Users.id).filter(Users.username == name).first()  # return (id,)
    if user_exist:
        exist = db.session.query(exists().where(neighbours.c.user_id == user_exist[0])).first()  # (True) or (False)
        exist_in_tracks_table = db.session.query(exists().where(UserTrackParade.user_id == user_exist[0])).first()
        if not exist[0]:
            return False
        elif not exist_in_tracks_table[0]:
            return False
        return True
    else:
        return False


def get_artist_hit_parade(name):
    filter_by_name = db.session.query(Users.id).filter(Users.username == name).first()  # (id,)
    artist_hit_parade = db.session.query(db.func.sum(UserHitParade.value), UserHitParade.artist) \
        .join(neighbours, UserHitParade.user_id == neighbours.c.neighbor_id) \
        .filter(neighbours.c.user_id == filter_by_name[0]) \
        .group_by(UserHitParade.artist).all()
    return artist_hit_parade


def get_track_hit_parade(name):
    filter_by_name = db.session.query(Users.id).filter(Users.username == name).first()  # (id,)
    track_hit_parade = db.session.query(db.func.sum(UserTrackParade.value), UserTrackParade.track, UserTrackParade.url)\
        .join(neighbours, UserTrackParade.user_id == neighbours.c.neighbor_id) \
        .filter(neighbours.c.user_id == filter_by_name[0]) \
        .group_by(UserTrackParade.track, UserTrackParade.url).all()
    return track_hit_parade


def artist_to_download(hit_parade):
    my_file = open('download/hitparade.csv', 'w+')  # clear the file
    my_file.close()
    count = 0
    with open('download/hitparade.csv', "wb") as csvf:
        csvwriter = csv.writer(csvf, delimiter=',')
        for value, name in sorted(hit_parade, reverse=True):
            count += 1
            csvwriter.writerow([str(count), name.encode('utf-8'), str(value)])  # count, name, value
    response = make_response(send_file('../download/hitparade.csv'))
    response.headers["Content-Disposition"] = "attachment; filename=hitparade.csv"
    return response


def track_to_download(hit_parade):
    count = 0
    with open('download/hitparade.csv', "wb") as csvf:
        csvwriter = csv.writer(csvf, delimiter=' ')
        for value, name, url in sorted(hit_parade, reverse=True):
            count += 1
            csvwriter.writerow([str(count), name.encode('utf-8'), str(value)])  # count, name, value
            csvwriter.writerow([str(url)])
    response = make_response(send_file('../download/hitparade.csv'))
    response.headers["Content-Disposition"] = "attachment; filename=hitparade.csv"
    return response


